# Masterthesis

Code used for the case study of the masterthesis from Alexander Wieland.
The code of the used methods are located in the folder code/.

The conda environment can be found in the environment.yml file
